#! /bin/bash
export LOGFILE=/var/log/startup.log
touch $LOGFILE

# Install DOCKER engine
yum install docker -y | tee -a $LOGFILE
service docker start | tee -a $LOGFILE
usermod -a -G docker ec2-user | tee -a $LOGFILE
chkconfig docker on | tee -a $LOGFILE

# GET context information
export INSTANCE_ID="`wget -qO- http://instance-data/latest/meta-data/instance-id`"
echo INSTANCE_ID: $INSTANCE_ID  | tee -a $LOGFILE

export REGION="`wget -qO- http://instance-data/latest/meta-data/placement/availability-zone | sed -e 's:\([0-9][0-9]*\)[a-z]*\$:\\1:'`"
echo REGION: $REGION  | tee -a $LOGFILE

export DOCKER_IMAGE="`aws ec2 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=docker_image" --region $REGION --output=text | cut -f5`"
echo DOCKER_IMAGE: $DOCKER_IMAGE  | tee -a $LOGFILE

export DOCKER_CONTAINER_PORT="`aws ec2 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=docker_container_port" --region $REGION --output=text | cut -f5`"
echo DOCKER_CONTAINER_PORT: $DOCKER_CONTAINER_PORT  | tee -a $LOGFILE

export EXPOSED_PORT="`aws ec2 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=exposed_port" --region $REGION --output=text | cut -f5`"
echo EXPOSED_PORT: $EXPOSED_PORT  | tee -a $LOGFILE

echo Great! Now, launching the image!  | tee -a $LOGFILE
docker run -d -p $EXPOSED_PORT:$DOCKER_CONTAINER_PORT $DOCKER_IMAGE | tee -a $LOGFILE

echo Wonderful! Should be OK now. Enjoy!  | tee -a $LOGFILE


