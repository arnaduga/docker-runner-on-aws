# Variables ZONE - Most likely to be changed

variable "environment" {
  type        = string
  default     = "dev"
  description = "If dev, the port 22 will be allowed, for development purposes. Otherwise, it won't"
}

variable "docker_image" {
  type        = string
  default     = "arnaduga/rental-receipt:latest"
  description = "Full image reference"
}

variable "docker_container_port" {
  type        = string
  default     = "5000"
  description = "Port exposed by the service INSIDE the container. Please refer to the image documentation"
}

variable "exposed_port" {
  type        = string
  default     = "80"
  description = "Your service port to be exposed. Usually 80 ou 443 fot http/https services"
}

variable "keypair" {
  type        = string
  default     = "ec2-august2020"
  description = "Keypair to use, with DEV environment only (null for != dev)"
}

# Varaible ZONE - Could stay unchanged

variable "region" {
  type        = "string"
  default     = "eu-west-1"
  description = "AWS region to be used"
}

variable "instance_type" {
  type        = string
  default     = "t2.nano"
  description = "Instance type to be ran"
}


# AWS Provider
provider "aws" {
  region = "${var.region}"
}


# Security group to allow connection to the EC2
resource "aws_security_group" "for_docker_runner" {
  name        = "for_docker_runner_${upper(var.environment)}"
  description = "Allow service inbound traffic"

  ingress {
    description = "Service Port from internet"
    from_port   = var.exposed_port
    to_port     = var.exposed_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "for_docker_runner"
    environment = "${upper(var.environment)}"
  }
}

resource "aws_security_group_rule" "open_ssh" {
  security_group_id = aws_security_group.for_docker_runner.id
  description       = "SSH service ingress for DEV purposes"
  count             = upper(var.environment) == "DEV" ? 1 : 0
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  cidr_blocks       = ["0.0.0.0/0"]
  protocol          = "tcp"
}

# Role to be assume by an EC2
resource "aws_iam_role" "get_tags_role" {
  name               = "get_tags_role"
  assume_role_policy = "${file("files/get_tags_role.json")}"
}

# Policy to get Tags
resource "aws_iam_role_policy" "get_tags_policy" {
  name = "get_tags_policy"
  role = aws_iam_role.get_tags_role.id

  policy = "${file("files/get_tags_policy.json")}"
}

# Instance profile to associate a role
resource "aws_iam_instance_profile" "get_tags_profile" {
  name = "get_tags_profile"
  role = aws_iam_role.get_tags_role.name
}

# Retrieved last Amazon Linux 2 image id
data "aws_ami" "amazon-linux-2-ami" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

}

# Create the instance
resource "aws_instance" "docker_runner" {
  ami                  = data.aws_ami.amazon-linux-2-ami.id
  instance_type        = "${var.instance_type}"
  key_name             = "${(upper(var.environment) == "DEV") ? "${var.keypair}" : null}"
  iam_instance_profile = aws_iam_instance_profile.get_tags_profile.name
  security_groups      = [aws_security_group.for_docker_runner.name]
  user_data            = "${file("files/startupScript.sh")}"

  tags = {
    Name                  = "Docker runner"
    builder               = "terraform"
    docker_image          = "${var.docker_image}"
    docker_container_port = "${var.docker_container_port}"
    exposed_port          = "${var.exposed_port}"
    environment           = "${upper(var.environment)}"
  }
}


output "dnsname" {
  value = "Your service DNS is ${aws_instance.docker_runner.public_dns}:${var.exposed_port}"
}

output "ip" {
  value = "Your service EC2 IP address is ${aws_instance.docker_runner.public_ip}"
}
