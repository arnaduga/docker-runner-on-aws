# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Unreleased

### Changed
- `TODO.md` file, with new ideas/improvements
- `README.md` file updates (finalizing the *how-to*)
- Updated the TF outputs

## [1.0.0] - 2020-08-23

### Added
- Initial project creation
