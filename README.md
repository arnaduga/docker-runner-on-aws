# Introduction

I recently create an entire CI process for a small NodeJS application in a Docker environment ([A rental receipt generator](https://gitlab.com/arnaduga/rental-receipt)).

However, it presents only a run on a local machine. So, I decided to script the creation and deployment on a tiny AWS EC2 machine (Amazon Linux 2), where Docker engine would be installed.

But that was not *re-usable* enough for me.

I decided then to it as generic as possible!

> The current repository contains parametric script to run any Docker image from Docker Hub in an EC2 machine



# Howto

## Pre-requisites

Your local environment MUST be ready to work with [Terraform](https://www.terraform.io/) and AWS.

To check if Terraform is appropriately installed:

```bash
$ terraform --version
Terraform v0.12.10

$
```

> Note: at this script creation date, version 0.13 of Terraform is alredy available. However, I prefered to keep version 0.12 for the moment. Future release will probably be in Terraform 0.13




## Prepare your parameters file

The `variable.tfvars` file is useless as-is, as it contains the variables list with default values.

You can either edit this file or duplicate it as per your need. 


### Parameter `environment`

*Default value*: `dev`

*Description*: This parameter is **NOT ONLY** used for naming and tagging: if valued `dev`, the created Security Group will also allow the port `22` for the SSH service. 

### Parameter `docker_image`

*Default value*: `arnaduga/rental-receipt:latest` (auto-promotion, I know)

*Description*: This parameter is the fullname of the image to be ran, from the [Docker Hub](https://hub.docker.com). 

A future version may also include [ECR](https://aws.amazon.com/ecr/) as source.

### Parameter `docker_container_port`

*Default value*: `5000`

*Description*: This is the port exposed *INSIDE* the image. This is **100%** dependent on the image you used (or you developed).

```mermaid
graph LR
  A[Browser] -->P([Port: 80]) --> B([Port: 5000])
  subgraph Container
    B --> C[Image with Service code]
  end
```

### Parameter `exposed_port`

*Default value*: `80`

*Description*: This parameter is **NOT ONLY** used to run the container correctly: it is also used for the Security Group, to open the appropriate port. 


### Parameter `keypair`

*Default value*: `ec2-august2020`

*Description*: This is the keypair name to be installed on the EC2 machine. This will be set *only* for the `dev` environment, for security reasons.

### Parameter `region`

*Default value*: `eu-west-1`

*Description*: This is the AWS Region. Update according to your need.

### Parameter `instance_type`

*Default value*: `t2.nano`

*Description*: This is the size of the EC2 machine to be built. If your application requires a lot of memory or compute, please adjust accordingly.

## Launch the Terraform script

Classically: 

`$ terraform plan -var-file variables.tfsvars` 

and if everything is OK: 

`$ terraform apply -var-file variables.tfsvars`

> That's it!

## Remove everything

Well, I won't teach you Terraform... Just read the doc or 

`$ terraform destroy`

(in the location of your `terraform.tfstate`... Keep it safe, but not on git!)


# Annex and tricky points

## Reading tags

Reading tags is NOT possible using the http://169.254.169.254 ... (WTF!).

To get them, startup script shell is using the `aws` CLI, command `describe-tags`.

The EC2 machine is able to call this CLI, because it assumes the role with `ec2:DescribeTags` policy statement (no ACCESSKEY/SECRETKEY required in that case)

*Note*: I'm not comfortable with the fact my EC2 instance can read ANY tags from EC2 service, not only its own. It tried to limit to EC2 itself with conditions:
```
    "Condition": {
        "StringEquals": {
            "aws:ARN": "${ec2:SourceInstanceARN}"
        }
    }
```

... but unsuccessfully. 


## Add the `ingress` for port 22 only for `dev`

Thanks to the resource `aws_security_group_rule` and the attribute `count = upper(var.environment) == "DEV" ? 1 : 0`

